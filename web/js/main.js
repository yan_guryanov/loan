var DataController = function (selector) {
    var me = this;
    
    this.mainElement = $(selector);
    this.emptyElem = me.mainElement.find('[data-empty]');
    this.headerElem = me.mainElement.find('[data-show]');
    this.links = me.mainElement.find('a');

    this.dataUrl = null;
    this.dataTemplate = null;
    this.createUrl = null;
    
    this.statuses = {
        'status-0': 'Pending',
        'status-1': 'Accepted',
        'status-2': 'Rejected'
    };

    this.loadData = function () {
        $.ajax({
            url: me.dataUrl,
            method: 'get',
            dataType: 'json',
            success: function (response) {
                if (!response.length) {
                    me.emptyElem.toggleClass('hidden');
                } else {
                    me.headerElem.toggleClass('hidden');
                }
                me.processData(response);
            },
            error: function (response) {
                alert(response.responseJSON.message);
            }
        });
    };
    
    this.sendCreateRequest = function (loanAmount, propertyValue, socialSecurity) {
        $.ajax({
            url: me.createUrl,
            method: 'post',
            dataType: 'json',
            data: JSON.stringify({
                amount: loanAmount, 
                property_value: propertyValue,
                social_security: socialSecurity
            }),
            success: function (response) {
                me.emptyElem.addClass('hidden');
                me.headerElem.removeClass('hidden');
                me.processData(new Array(response), false);
            },
            error: function (response) {
                alert(response.responseJSON.message);
            }
        });
    };
    
    this.matchStatus = function (status) {
        var key = 'status-' + status;
        if (key in me.statuses) {
            return me.statuses[key];
        }
        
        return '';
    };
    
    this.processData = function (data, append) {
        append = typeof append === 'undefined' ? true : !!append;
        $.each(data, function(index, dataElement) {
            var $tmpl = me.dataTemplate.clone();
            if (append) {
                $tmpl.insertBefore(me.dataTemplate);
            } else {
                $tmpl.insertAfter(me.headerElem);
            }
            
            $tmpl.find('[data-id]').html(dataElement['id']);
            $tmpl.find('[data-status]').html(me.matchStatus(dataElement['status']));
            $tmpl.removeClass('hidden');
        });
    };
    
    this.init = function () {
        if (me.mainElement.length) {
            me.dataTemplate = me.mainElement.find('[data-template]');
            me.dataUrl = me.mainElement.data("url");
            me.createUrl = me.mainElement.data("create-url");
            me.loadData();
        }

        me.links.click(function () {
            var url = $(this).attr('href');
            
            return false;
        });
    };
    
    this.init();
    
    return {
        addData: function (loanAmount, propertyValue, socialSecurity) {
            me.sendCreateRequest(loanAmount, propertyValue, socialSecurity);
        }
    };
};

(function ($) {
    var dataLoader = new DataController('#data-box');

    $('#loanRequestModal')
        .on('submit', 'form', function (e) {
            e.preventDefault();
            dataLoader.addData(
                $('#loanAmount').val(),
                $('#propertyValue').val(),
                $('#socialSecurity').val()
            );
            $('#loanRequestModal').modal('hide');
        })
        .on('hidden.bs.modal', function () {
            $(this).find('form').get(0).reset();
        });
})(jQuery);

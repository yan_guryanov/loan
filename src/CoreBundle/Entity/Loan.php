<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="loan")
 * @ORM\Entity(repositoryClass="CoreBundle\Repository\ORM\LoanRepository")
 */
class Loan
{
    const STATUS_PENDING  = 0;
    const STATUS_ACCEPTED = 1;
    const STATUS_REJECTED = 2;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float")
     */
    private $amount;

    /**
     * @var float
     *
     * @ORM\Column(name="property_value", type="float")
     */
    private $propertyValue;

    /**
     * @var string
     *
     * @ORM\Column(name="social_security", type="string", length=255)
     */
    private $socialSecurity;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status = self::STATUS_PENDING;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @param float $amount
     * @param float $propertyValue
     * @param float $socialSecurity
     */
    public function __construct($amount, $propertyValue, $socialSecurity)
    {
        $this->amount         = (float) $amount;
        $this->propertyValue  = (float) $propertyValue;
        $this->socialSecurity = $socialSecurity;
        $this->createdAt      = new \DateTime();
    }

    /**
     * Set status to accepted
     */
    public function makeAccepted()
    {
        $this->status = self::STATUS_ACCEPTED;
    }

    /**
     * Set status to rejected
     */
    public function makeRejected()
    {
        $this->status = self::STATUS_REJECTED;
    }

    /**
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return float 
     */
    public function getFormattedAmount()
    {
        return number_format($this->amount, 2, '.', ',');
    }

    /**
     * @return float 
     */
    public function getPropertyValue()
    {
        return $this->propertyValue;
    }

    /**
     * @return float 
     */
    public function getPropertyValueFormatted()
    {
        return number_format($this->propertyValue, 2, '.', ',');
    }

    /**
     * @return string 
     */
    public function getSocialSecurity()
    {
        return $this->socialSecurity;
    }

    /**
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return integer 
     */
    public function isPending()
    {
        return $this->status === self::STATUS_PENDING;
    }

    /**
     * @return integer 
     */
    public function isAccepted()
    {
        return $this->status === self::STATUS_ACCEPTED;
    }

    /**
     * @return integer 
     */
    public function isRejected()
    {
        return $this->status === self::STATUS_REJECTED;
    }

    /**
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}

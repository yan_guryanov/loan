<?php

namespace CoreBundle\Domain;

use CoreBundle\Entity\Loan;

interface LoansInterface
{
    /**
     * @param int $id
     * @return Loan
     */
    public function getLoan($id);

    /**
     * @return Loan[]
     */
    public function getLoans();

    /**
     * @param Loan $loan
     * @param float $acceptableRatio
     */
    public function createLoan(Loan $loan, $acceptableRatio);
}
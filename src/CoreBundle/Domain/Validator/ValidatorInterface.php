<?php

namespace CoreBundle\Domain\Validator;

use CoreBundle\Exception\InvalidDataException;

interface ValidatorInterface
{
    /**
     * @param array $data
     * @throws InvalidDataException
     */
    public function validate(array $data);
}
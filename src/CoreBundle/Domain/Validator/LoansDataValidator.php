<?php

namespace CoreBundle\Domain\Validator;

use CoreBundle\Exception\InvalidDataException;

class LoansDataValidator implements ValidatorInterface
{
    /**
     * @var array
     */
    private static $requiredFields = [
        'amount',
        'property_value',
        'social_security',
    ];
    
    /**
     * {@inheritdoc}
     */
    public function validate(array $data)
    {
        foreach (self::$requiredFields as $requiredField) {
            if (!isset($data[$requiredField])) {
                throw new InvalidDataException(sprintf('Field %s is required', $requiredField));
            }
        }

        if ((float) $data['amount'] <= 0) {
            throw new InvalidDataException('Field `amount` must be greater than 0');
        }
        
        if ((float) $data['property_value'] <= 0) {
            throw new InvalidDataException('Field `property_value` must be greater than 0');
        }

        if (empty(trim($data['social_security']))) {
            throw new InvalidDataException('Field `social_security` cannot be empty');
        }
    }
}
<?php

namespace CoreBundle\Domain;

use CoreBundle\Entity\Loan;
use CoreBundle\Repository\LoanRepositoryInterface;

class Loans implements LoansInterface
{
    /**
     * @var LoanRepositoryInterface
     */
    private $loadRepository;

    /**
     * @param LoanRepositoryInterface $loadRepository
     */
    public function __construct(LoanRepositoryInterface $loadRepository)
    {
        $this->loadRepository = $loadRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function getLoan($id)
    {
        return $this->loadRepository->findById($id);
    }

    /**
     * {@inheritdoc}
     */
    public function getLoans()
    {
        return $this->loadRepository->findAll();
    }

    /**
     * {@inheritdoc}
     */
    public function createLoan(Loan $loan, $acceptableRatio)
    {
        $currentRatio = $loan->getPropertyValue() / $loan->getAmount(); 
        if ($currentRatio >= $acceptableRatio) {
            $loan->makeAccepted();
        } else {
            $loan->makeRejected();
        }
        
        $this->loadRepository->save($loan);
    }
}
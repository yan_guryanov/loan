<?php

namespace CoreBundle\Repository;

use CoreBundle\Entity\Loan;
use CoreBundle\Exception\NotFoundException;

interface LoanRepositoryInterface
{
    /**
     * @param int $id
     * @return Loan
     * @throws NotFoundException
     */
    public function findById($id);

    /**
     * @return Loan[]
     */
    public function findAll();

    /**
     * @param Loan $loan
     */
    public function save(Loan $loan);
}
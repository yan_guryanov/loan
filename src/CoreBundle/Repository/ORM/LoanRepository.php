<?php

namespace CoreBundle\Repository\ORM;

use CoreBundle\Entity\Loan;
use CoreBundle\Exception\NotFoundException;
use CoreBundle\Repository\LoanRepositoryInterface;
use Doctrine\ORM\EntityRepository;

class LoanRepository extends EntityRepository implements LoanRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function findById($id)
    {
        $loan = parent::find($id);
        if (!$loan) {
            throw new NotFoundException('Loan not found');
        }
        
        return $loan;
    }

    /**
     * {@inheritdoc}
     */
    public function findAll()
    {
        return parent::findBy([], ['createdAt' => 'DESC']);
    }

    /**
     * {@inheritdoc}
     */
    public function save(Loan $loan)
    {
        $this->getEntityManager()->persist($loan);
        $this->getEntityManager()->flush();
    }
}

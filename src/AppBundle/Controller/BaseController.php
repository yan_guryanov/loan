<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BaseController extends Controller
{
    /**
     * @Route("/", name="mainpage")
     * @Method("GET")
     * 
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('AppBundle:Base:index.html.twig');
    }
}
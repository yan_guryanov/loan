<?php

namespace AppBundle\Controller\Json;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Encoder\JsonDecode;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

abstract class AbstractJsonController extends Controller
{
    /**
     * @param string $content
     * @return array
     */
    protected function jsonToArray($content)
    {
        return (new JsonDecode(true))->decode($content, JsonEncoder::FORMAT);
    }
}

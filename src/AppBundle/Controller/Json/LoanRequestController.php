<?php

namespace AppBundle\Controller\Json;

use CoreBundle\Exception\InvalidDataException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Application\LoanInterface;
use Symfony\Component\Serializer\Exception\UnexpectedValueException;

class LoanRequestController extends AbstractJsonController
{
    /**
     * @Route("/loans", name="loan_list")
     * @Method("GET")
     * 
     * @return Response
     */
    public function loanRequestAction()
    {
        $response = new JsonResponse();
        try {
            $loans = $this->getLoanService()->getLoansList();

            $response->setData($loans);
            $response->setStatusCode(Response::HTTP_OK);
        } catch (UnexpectedValueException $e) {
            $response->setData(['message' => $e->getMessage()]);
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);
        } catch (InvalidDataException $e) {
            $response->setData(['message' => $e->getMessage()]);
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            $response->setData(['message' => 'Internal server error']);
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        
        return $response;
    }

    /**
     * @Route("/loan", name="request_loan")
     * @Method("POST")
     * 
     * @param Request $request
     * @return Response
     */
    public function loansRequestListAction(Request $request)
    {
        $response = new JsonResponse();
        try {
            $data = $this->jsonToArray($request->getContent());
            $loan = $this->getLoanService()->createLoanRequest($data);

            $response->setData($loan);
            $response->setStatusCode(Response::HTTP_OK);
        } catch (UnexpectedValueException $e) {
            $response->setData(['message' => $e->getMessage()]);
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);
        } catch (InvalidDataException $e) {
            $response->setData(['message' => $e->getMessage()]);
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            $response->setData(['message' => 'Internal server error']);
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        
        return $response;
    }

    /**
     * @return LoanInterface
     */
    private function getLoanService()
    {
        return $this->get('app.application.loan');
    }
}

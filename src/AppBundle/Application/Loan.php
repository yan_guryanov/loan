<?php

namespace AppBundle\Application;

use CoreBundle\Domain\LoansInterface;
use CoreBundle\Domain\Validator\ValidatorInterface;
use CoreBundle\Entity\Loan as LoanEntity;

class Loan implements LoanInterface
{
    /**
     * @var LoansInterface
     */
    private $loansService;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var float
     */
    private $acceptableRatio;

    /**
     * @param LoansInterface        $loansService
     * @param ValidatorInterface    $validator
     * @param float                 $acceptableRatio
     */
    public function __construct(
        LoansInterface $loansService, 
        ValidatorInterface $validator,
        $acceptableRatio
    ) {
        $this->loansService    = $loansService;
        $this->validator       = $validator;
        $this->acceptableRatio = $acceptableRatio;
    }

    /**
     * {@inheritdoc}
     */
    public function getLoan($id)
    {
        return $this->loansService->getLoan($id);
    }

    /**
     * {@inheritdoc}
     */
    public function getLoansList()
    {
        $loans = [];
        foreach ($this->loansService->getLoans() as $loan) {
            $loans[] = [
                'id'     => $loan->getId(),
                'status' => $loan->getStatus(),
            ];    
        }
        
        return $loans;
    }

    /**
     * {@inheritdoc}
     */
    public function createLoanRequest(array $data)
    {
        $this->validator->validate($data);
        $loan = new LoanEntity(
            $data['amount'], 
            $data['property_value'], 
            $data['social_security']
        );
            
        $this->loansService->createLoan($loan, $this->acceptableRatio);
        
        return [
            'id'     => $loan->getId(),
            'status' => $loan->getStatus(),
        ];
    }
}
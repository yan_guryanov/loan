<?php

namespace AppBundle\Application;

use CoreBundle\Entity\Loan;

interface LoanInterface
{
    /**
     * @param int $id
     * @return array
     */
    public function getLoan($id);

    /**
     * @return array
     */
    public function getLoansList();

    /**
     * @param array $data
     * @return Loan
     */
    public function createLoanRequest(array $data);
}
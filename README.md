# README #

This README would normally document whatever steps are necessary to get your application up and running.

I don't know about version of PHP, so I wrote this code for PHP 5.3 for better compatibility. 

### What is this repository for? ###

Creation of a loan submission Client interface & API.

### How do I get set up? ###

* Run this in **<project-directory>** to download project:

```
#!shell

    git clone git@bitbucket.org:yan_guryanov/loan.git
```

* Next install dependencies:

```
#!shell

    php composer.phar install
```
Note: During dependencies installation  script prompt you to enter mysql connection parameters.

* If the database has not been created, run following command (otherwise skip this step):

```
#!shell

    php app/console doctrine:database:create
```

* Create database schema:

```
#!shell

php app/console doctrine:schema:create
```

Note: On this step it'll be better to use migrations, but for test task it's overhead

* Setup web-server for directory **<project-directory>/web**